import requests as req
import demjson

params = {"c0-scriptName": "CardapioControleDWR", 
          "c0-methodName": "obterCardapioRestUSP",
          "c0-id": 0,
          "c0-param0": "string:8",
          "batchId": 1,
          "instanceId": 0,
          "scriptSessionId": "aaaaa",
          "page": "aaaa",
          "windowName": "",
          "callCount": "1"}


r = req.post("https://uspdigital.usp.br/rucard/dwr/call/plaincall/CardapioControleDWR.obterCardapioRestUSP.dwr", data=params)

bruto = r.text
raw_json = "[" + bruto.split("[")[2].split("]")[0] + "]"
cardapio = demjson.decode(raw_json)
print(cardapio)