
# coding: utf-8

# In[10]:


import requests as req
import demjson
import json
import datetime
import os


# In[11]:


default_output_path = os.path.expanduser("~/bandejao.weirdjson")


# In[4]:


def obter_dados(restaurante_id, method_name):
    cardapio_url = "https://uspdigital.usp.br/rucard/dwr/call/plaincall/"
    cardapio_url += "CardapioControleDWR.obterCardapioRestUSP.dwr"
    params = {"c0-scriptName": "CardapioControleDWR", 
          "c0-methodName": method_name,
          "c0-id": 0,
          "c0-param0": "string:{}".format(restaurante_id),
          "batchId": 1,
          "instanceId": 0,
          "scriptSessionId": "aaaaa",
          "page": "aaaa",
          "windowName": "",
          "callCount": "1"}
    r = req.post(cardapio_url, data=params)
    bruto = r.text
    raw_json = "[" + bruto.split("[")[2].split("]")[0] + "]"
    json = demjson.decode(raw_json)
    return json


# In[13]:


def obter_cardapio(rest_id):
    return obter_dados(rest_id, "obterCardapioRestUSP")

def obter_restaurante(rest_id):
    return obter_dados(rest_id, "obterRestauranteUsp")

def iter_cardapio():
    output = []
    for i in range(25):
        cardapio = obter_cardapio(i)
        restaurante = obter_restaurante(i)
        obj = {"time": str(datetime.datetime.now()), "cardapio": cardapio, "restaurante": restaurante}
        output.append(obj)
    return output

def main():
    output = iter_cardapio()
    json_output = json.dumps(output)
    with open(default_output_path, "a") as fid:
        fid.write("\n")
        fid.write(json_output)
    print(json_output)
    
if __name__ == "__main__":
    main()

