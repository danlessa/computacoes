@0001 Ano      $4.    /* Ano de referência */
@0005 Trimestre      $1.    /* Trimestre de referência */
@0006 UF      $2.    /* Unidade da Federação */
@0008 Capital      $2.    /* Município da Capital */
@0010 RM_RIDE      $2.    /* Reg. Metr. e Reg. Adm. Int. Des. */
@0012 UPA      $9.    /* Unidade Primária de Amostragem */
@0021 Estrato      $8.    /* Estrato */
@0028 V1008      $2.    /* Número de seleção do domicílio */
@0030 V1014      $2.    /* Painel */
@0032 V1022      $1.    /* Tipo de situação da região */
@0033 V1023      $1.    /* Tipo de área */
@0034 V1030       9.    /* Projeção da população */
@0043 V1031       15.    /* Peso SEM pós estratificação */
@0058 V1032       15.    /* Peso COM pós estratificação */
@0073 posest      $3.    /* Domínios de projeção */
@0076 V2001       2.    /* Número de pessoas no domicílio */
@0078 V2003      $2.    /* Número de ordem */
@0080 V2005      $2.    /* Condição no domicílio */
@0082 V2007      $1.    /* Sexo */
@0083 V2008      $2.    /* Dia de nascimento */
@0085 V20081      $2.    /* Mês de nascimento */
@0087 V20082      $4.    /* Ano de nascimento */
@0091 V2009       3.    /* Idade na data de referência */
@0094 V2010      $1.    /* Cor ou raça */
@0095 V3001      $1.    /* Sabe ler e escrever */
@0096 V3002      $1.    /* Frequenta escola */
@0097 V3002A      $1.    /* A escola que ... frequenta é de */
@0098 V3003A      $2.    /* Qual é o curso que frequenta */
@0100 V3004      $1.    /* Duração deste curso que requenta */
@0101 V3005A      $1.    /* Curso que freq é organizado em: */
@0102 V3006      $2.    /* Qual é o ano/série que frequenta */
@0104 V3007      $1.    /* Concluiu outro curso de graduação */
@0105 V3008      $1.    /* Anteriormente frequentou escola */
@0106 V3009A      $2.    /* Curso mais elevado que frequentou */
@0108 V3010      $1.    /* Duração do curso que frequentou */
@0109 V3011A      $1.    /* Curso que freq é organizado em: */
@0110 V3012      $1.    /* Aprovado na prim. série do curso */
@0111 V3013      $2.    /* Último ano/série que concluiu */
@0113 V3014      $1.    /* Concluiu o curso que frequentou */
@0114 V4001      $1.    /* Trabalhou 1 hr em ativ. remunerd. */
@0115 V4002      $1.    /* Trabalhou 1 hr em produtos etc... */
@0116 V4003      $1.    /* Fez algum bico pelo menos de 1 hr */
@0117 V4004      $1.    /* Ajudou sem receber no domic. 1 hr */
@0118 V4005      $1.    /* Afastado trabalho remunerado */
@0119 V4006A      $1.    /* Motivo de estar afastado */
@0120 V4008      $1.    /* Quanto tempo que estava afastado */
@0121 V40081      $2.    /* Tempo de afastamenento até 1 ano */
@0123 V40082      $2.    /* Tempo de afastamen. de 1 a 2 anos */
@0125 V40083      $2.    /* Tempo de afastamen. mais d 2 anos */
@0127 V4009      $1.    /* Quantos trabalhos tinhana semana */
@0128 V4010      $4.    /* Ocupação no trab. principal */
@0132 V4012      $1.    /* Posição na ocupação */
@0133 V40121      $1.    /* Tipo trabalhador não remunerado */
@0134 V4013      $5.    /* Atividade no trab. principal */
@0139 V40132A      $1.    /* Seção da atividade */
@0140 V4014      $1.    /* Esse trabalho era na área */
@0141 V4019      $1.    /* Negócio/empresa registrado no CNPJ*/
@0142 V4024      $1.    /* Serv. domést. em mais de 1 domic. */
@0143 V4025      $1.    /* Contratado como empreg. temporário*/
@0144 V4026      $1.    /* Era contratado somente por pessoa responsável pelo negócio */
@0145 V4027      $1.    /* Era contratado somente por intermediário */
@0146 V4028      $1.    /* Servidor público estatutário */
@0147 V4029      $1.    /* Carteira de trabalho assinada */
@0148 V4032      $1.    /* Contribuinte de instit. d previd. */
@0149 V4033      $1.    /* Rendimento habitual var. auxil. */
@0150 V40331      $1.    /* Rendimento habitual em dinheiro */
@0151 V403311      $1.    /* Faixa do valor do rendimento hab. */
@0152 V403312       8.    /* Valor do rend. hab. em dinheiro */
@0160 V40332      $1.    /* Rendimento habitual em produtos */
@0161 V403321      $1.    /* Faixa do valor do rendimento hab. */
@0162 V403322       8.    /* Valor do rend. hab. em produtos */
@0170 V40333      $1.    /* Rendimento habitual em benefícios */
@0171 V403331      $1.    /* Tipo rend. habitual em benefícios */
@0172 V4034      $1.    /* Rendimento efetivo var. auxil. */
@0173 V40341      $1.    /* Rendimento efetivo em dinheiro */
@0174 V403411      $1.    /* Faixa do valor do rendimento efe. */
@0175 V403412       8.    /* Valor do rend. efe. em dinheiro */
@0183 V40342      $1.    /* Rendimento efetivo em produtos */
@0184 V403421      $1.    /* Faixa do valor do rendimento efe. */
@0185 V403422       8.    /* Valor do rend. efe. em produtos */
@0193 V4039       3.    /* Hrs habituais no trab. princ. */
@0196 V4039C       3.    /* Hrs efetivas no trab. princ. */
@0199 V4040      $1.    /* Tempo que estava nesse trabalho */
@0200 V40401       2.    /* De 1 mês a menos de 1 ano */
@0202 V40402       2.    /* De 1 ano a menos de 2 anos */
@0204 V40403       2.    /* De 2 anos ou mais tempo */
@0206 V4041      $4.    /* Ocupação no trab. secundário */
@0210 V4043      $1.    /* Posição na ocupação */
@0211 V40431      $1.    /* Tipo trabalhador não remunerado */
@0212 V4044      $5.    /* Atividade no trab. secundário */
@0217 V4045      $1.    /* Esse trabalho era na área */
@0218 V4046      $1.    /* Negócio/empresa registrado no CNPJ*/
@0219 V4047      $1.    /* Servidor público estatutário */
@0220 V4048      $1.    /* Carteira de trabalho assinada */
@0221 V4049      $1.    /* Contribuinte de instit. d previd. */
@0222 V4050      $1.    /* Rendimento habitual var. auxil. */
@0223 V40501      $1.    /* Rendimento habitual em dinheiro */
@0224 V405011      $1.    /* Faixa do valor do rendimento hab. */
@0225 V405012       8.    /* Valor do rend. hab. em dinheiro */
@0233 V40502      $1.    /* Rendimento habitual em produtos */
@0234 V405021      $1.    /* Faixa do valor do rendimento hab. */
@0235 V405022       8.    /* Valor do rend. hab. em produtos */
@0243 V40503      $1.    /* Rendimento habitual em benefícios */
@0244 V405031      $1.    /* Tipo rend. habitual em benefícios */
@0245 V4051      $1.    /* Rendimento efetivo var. auxil. */
@0246 V40511      $1.    /* Rendimento efetivo em dinheiro */
@0247 V405111      $1.    /* Faixa do valor do rendimento efe. */
@0248 V405112       8.    /* Valor do rend. efe. em dinheiro */
@0256 V40512      $1.    /* Rendimento efetivo em produtos */
@0257 V405121      $1.    /* Faixa do valor do rendimento efe. */
@0258 V405122       8.    /* Valor do rend. efe. em produtos */
@0266 V4056       3.    /* Hrs habituais no trab. secun. */
@0269 V4056C       3.    /* Hrs efetivas no trab. secun. */
@0272 V4057      $1.    /* Contribuinte de instit. d previd. */
@0273 V4058      $1.    /* Rendimento habitual var. auxil. */
@0274 V40581      $1.    /* Rendimento habitual em dinheiro */
@0275 V405811      $1.    /* Faixa do valor do rendimento hab. */
@0276 V405812       8.    /* Valor do rend. hab. em dinheiro */
@0284 V40582      $1.    /* Rendimento habitual em produtos */
@0285 V405821      $1.    /* Faixa do valor do rendimento hab. */
@0286 V405822       8.    /* Valor do rend. hab. em produtos */
@0294 V40583      $1.    /* Rendimento habitual em benefícios */
@0295 V405831      $1.    /* Tipo rend. habitual em benefícios */
@0296 V40584      $1.    /* Não remunerado */
@0297 V4059      $1.    /* Rendimento efetivo var. auxil. */
@0298 V40591      $1.    /* Rendimento efetivo em dinheiro */
@0299 V405911      $1.    /* Faixa do valor do rendimento efe. */
@0300 V405912       8.    /* Valor do rend. efe. em dinheiro */
@0308 V40592      $1.    /* Rendimento efetivo em produtos */
@0309 V405921      $1.    /* Faixa do valor do rendimento efe. */
@0310 V405922       8.    /* Valor do rend. efe. em produtos */
@0318 V4062       3.    /* Hrs habituais no(s) outro(s) trab.*/
@0321 V4062C       3.    /* Hrs efetivas no(s) outro(s) trab .*/
@0324 V4063A      $1.    /* Gostaria trabalhar + hrs habituais*/
@0325 V4064A      $1.    /* Dispon. trabalhar + hrs habituais */
@0326 V4071      $1.    /* Providência p/ conseg. trab.(30d) */
@0327 V4072A      $1.    /* Principal provid. p/conseg. trab. */
@0328 V4073      $1.    /* Gostaria de ter trabalhado */
@0329 V4074A      $2.    /* Motivo de não ter tomado provid. */
@0331 V4075A      $1.    /* Tempo em que irá começar o trab. */
@0332 V4075A1      $2.    /* Meses em que irá começar o trab. */
@0334 V4076      $1.    /* Tempo tentando conseguir trabalho */
@0335 V40761       2.    /* Tempo tentando trab. 1 mes-1 ano */
@0337 V40762       2.    /* Tempo tentando trab. 1 ano-2 anos */
@0339 V40763       2.    /* Tempo tentando trab. + de 2 anos */
@0341 V4077      $1.    /* Poderia ter começado a trabalhar */
@0342 V4078A      $1.    /* Motivo p /ñ querer/começar a trab. */
@0343 V4082      $1.    /* Trab por pelo menos 1 hora em 1 ano*/
@0344 V4099      $1.    /* Exerceu ativ. Caça, pesca, etc prop. cons. */
@0345 V4100       3.    /* Hrs. Dest. p/ ativ. */
@0348 V4101      $5.    /* Atividade */
@0353 V4102      $1.    /* Exerceu Ativ. Corte ou coleta lenha, etc prop. cons. */
@0354 V4103       3.    /* Hrs. Dest. p/ ativ. */
@0357 V4104      $5.    /* Atividade */
@0362 V4105      $1.    /* Exerceu Ativ. Fabric. Roupas, tricô, etc prop. Cons. */
@0363 V4106       3.    /* Hrs. Dest. p/ ativ. */
@0366 V4107      $5.    /* Atividade */
@0371 V4108      $1.    /* Exerceu Ativ.  Construc. De casa, etc prop. Cons. */
@0372 V4109       3.    /* Hrs. Dest. p/ ativ. */
@0375 V4110      $5.    /* Atividade */
@0380 V4111      $1.    /* Trab volunt. pelo menos 1 hr. sem remun/ */
@0381 V41111      $1.    /* Trab volunt. pelo menos 1 hr. sem remun/: congreg. Relig., sindicato, condim., etc */
@0382 V41112      $1.    /* Trab volunt. pelo menos 1 hr. sem remun/: ONG, assis. Morad, etc. */
@0383 V41113      $1.    /* Trab volunt. pelo menos 1 hr. sem remun/: morad. Tarefas domesticas */
@0384 V41114      $1.    /* Trab volunt. pelo menos 1 hr. sem remun/: meio amb. */
@0385 V41115      $1.    /* Trab volunt. pelo menos 1 hr. sem remun/: ñ morad. Tarefas domesticas */
@0386 V41116      $1.    /* Trab volunt. pelo menos 1 hr. sem remun/: ñ morad. Serv. Prof. */
@0387 V41117      $1.    /* Trab volunt. pelo menos 1 hr. sem remun/: outro trab. Volunt. */
@0388 V4111A      $1.    /* Freq. Trab. Volunt. */
@0389 V4112       3.    /* Hrs. Trab. Voluntário s/ remun. */
@0392 V4113      $4.    /* Tarefa */
@0396 V4114      $1.    /* Trab p/ empresa, organ. Ou institut. */
@0397 V4115      $5.    /* Atividade */
@0402 V4117A      $1.    /* Cuidou de parentes que moradores */
@0403 V4117A1      $1.    /* Cuidou de parentes que moradores: pessoais */
@0404 V4117A2      $1.    /* Cuidou de parentes que moradores: educacional */
@0405 V4117A3      $1.    /* Cuidou de parentes que moradores: entreter */
@0406 V4117A4      $1.    /* Cuidou de parentes que moradores: monitorar */
@0407 V4117A5      $1.    /* Cuidou de parentes que moradores:locomoção */
@0408 V4117A6      $1.    /* Cuidou de parentes que moradores */
@0409 V4119      $1.    /* Cuidou de parentes que não moradores */
@0410 V4120      $1.    /* Fez tar. domésticas para o próprio dom. */
@0411 V41201      $1.    /* Fez tar. domésticas para o próprio dom.: Cozinha */
@0412 V41202      $1.    /* Fez tar. domésticas para o próprio dom.: vestuário */
@0413 V41203      $1.    /* Fez tar. domésticas para o próprio dom.: manutenção */
@0414 V41204      $1.    /* Fez tar. domésticas para o próprio dom.: limpeza */
@0415 V41205      $1.    /* Fez tar. domésticas para o próprio dom.: organização */
@0416 V41206      $1.    /* Fez tar. domésticas para o próprio dom.: compras */
@0417 V41207      $1.    /* Fez tar. domésticas para o próprio dom.: animais */
@0418 V41208      $1.    /* Fez tar. domésticas para o próprio dom.: outras tarefas */
@0419 V4121A      $1.    /* Fez tarefa doméstica em dom. de parente */
@0420 V4121B       3.    /* Hrs de ativi. de cuidados de pessoas / afazeres domésticos */
@0423 V5001A      $1.    /* Recebeu BPC-LOAS */ 
@0424 V5001A2       8.    /* Rend recebido de BPC-LOAS */
@0432 V5002A      $1.    /* Recebeu bolsa família */ 
@0433 V5002A2       8.    /* Rend recebido de bolsa familia */
@0441 V5003A      $1.    /* Recebeu outro prog social */
@0442 V5003A2       8.    /* Rend recebido de outro prog social */
@0450 V5004A      $1.    /* Recebeu aposentadoria e pensão */
@0451 V5004A2       8.    /* Rend recebido de aposentadoria e pensão */
@0459 V5006A      $1.    /* Recebeu pensão alimentícia, doação, etc */
@0460 V5006A2       8.    /* Rend recebido por pensão alimentícia, doação, etc */
@0468 V5007A      $1.    /* Recebeu aluguel e arrendamento */
@0469 V5007A2       8.    /* Rend recebido aluguel e arrendamento */
@0477 VD2002      $2.    /* Condição no domicílio */
@0479 VD2003       2.    /* Número de componentes do domic. */
@0481 VD2004      $1.    /* Espécie da unidade doméstica*/
@0482 VD3001      $1.    /* Nível de instrução */
@0483 VD3002      $2.    /* Anos de estudo (5 anos ou mais de idade) */
@0485 VD4001      $1.    /* Condição em relação força d trab. */
@0486 VD4002      $1.    /* Condição de ocupação */
@0487 VD4003      $1.    /* Força de trabalho potencial */
@0488 VD4004      $1.    /* Subocupação por insuficiên. de hrs*/
@0489 VD4005      $1.    /* Pessoas desalentadas */
@0490 VD4007      $1.    /* Posição na ocupação trab. princ. */
@0491 VD4008      $1.    /* Posição na ocupação trab. princ. */
@0492 VD4009      $2.    /* Posição na ocupação trab. princ. */
@0494 VD4010      $2.    /* Grupamen. d ativid. trab. princ. */
@0496 VD4011      $2.    /* Grupamen. ocupacion. trab. Princ. */
@0498 VD4012      $1.    /* Contrib. instit. previd. qq trab. */
@0499 VD4013      $1.    /* Faixa hrs habituais em todos trab. */
@0500 VD4014      $1.    /* Faixa hrs efetivas em todos trab. */
@0501 VD4015      $1.    /* Tipo d remuneração trab. princ. */
@0502 VD4016       8.    /* Rendim. habitual trab. princ. */
@0510 VD4017       8.    /* Rendim. efetivo trab. princ. */
@0518 VD4018      $1.    /* Tipo d remuneração em qq trabalho */
@0519 VD4019       8.    /* Rendim. habitual qq trabalho */
@0527 VD4020       8.    /* Rendim. efetivo qq trabalho */
@0535 VD4022       8.    /* Rendim. Efetivo tds fontes */
@0543 VD4030      $1.    /* Pq ñ proc./ñ gost./ñ disp.p/trab. */
@0544 VD4031       3.    /* Hrs habituais em todos trab. */
@0547 VD4035       3.    /* Hrs efetivas em todos trab. */
@0550 VD4036      $1.    /* Faixa hrs habituais trab. princ. */
@0551 VD4037      $1.    /* Faixa hrs efetivas trab. princ. */
@0552 VD4039      $1.    /* Cuidado de moradores do dom. ou fora do dom. */
@0553 VD4040      $1.    /* Cuidado de moradores de 0 a 5 anos do domicílio */
@0554 VD4041      $1.    /* Cuidado de moradores de 15 a 59 anos do domicílio */
@0555 VD4042      $1.    /* Cuidado de moradores de 6 a 14 anos do domicílio */
@0556 VD4043      $1.    /* Cuidado de moradores de 60 anos ou mais do domicílio */
@0557 VD4047       8.    /* Rend efetivo de progr sociais, seguro-desemprego, etc */
@0565 VD4048       8.    /* Rend efetivo recebido de outras fontes */
@0573 VD4049      $1.    /* Fez tarefas domést. p/ o próprio dom. ou em dom. de parente */
@0574 VD5001       8.    /* Rend efetivo domiciliar */
@0582 VD5002       8.    /* Rend efetivo domiciliar per capita */
@0590 VD5003      $1.    /* Faixa de rend efetivo domiciliar per capita */
@0591 VD5004       8.    /* Rend efetivo domiciliar */
@0599 VD5005       8.    /* Rend efetivo domiciliar per capita */
@0607 VD5006      $1.    /* Faixa de rend efetivo domiciliar per capita */
