"""
Neste programa, recebe-se de input um nome de arquivo contendo uma matriz de zeros e um que representam um labirinto. O programa então faz uma lista de todas as 'portas de entrada' possíveis para esse labirinto e verifica os caminhos possíveis bem como a distância entre cada elemento de matriz e a porta de entrada.

Execução: python3 ep.py3 arquivo.txt


Funcoes neste código:

lerMatriz(caminho_arquivo)
obterElemento(matriz, localizacao)
modificarElemento(matriz, localizacao, valor)
obterBordas(matriz)
checarLocalizacao(matriz, localizacao)
descobrirVizinhos(matriz, localizacao)
numeraVizinhos(lab, n, m, k)
imprimirMaisDistante(lab, n, m, dist)
ilustrarMatriz(matriz)
gerarCaminho(matriz, porta)
main(caminho_arquivo="teste.txt")
"""

import sys


def lerMatriz(caminho_arquivo):
    """
    Função para ler a matriz do labirinto dado um parâmetro indicando
    a localização do arquivo. Retorna None em caso de erro ou uma lista
    2d com a matriz.
    """
    linhas = None
    
    # Tentar ler o arquivo e armazenar as linhas
    try:
        with open(caminho_arquivo, "r") as arquivo:
            linhas = arquivo.readlines()
    except:
        print("Erro ao abrir o arquivo")
        return None
    
    try:    
        matriz = []
        for linha in linhas:
            elemento_linha = []  
            colunas = linha.split(" ")
            for elemento in colunas:                
                # Tirar caracteres indesejados e converter para número
                elemento = int(elemento.strip())
                
                elemento_linha.append(elemento)
            matriz.append(elemento_linha)
        return matriz
    
    except:
        print("Erro ao interpretar o arquivo")
        return None

    
def obterElemento(matriz, localizacao):
    """
    Obtém o elemento da matriz na localização dada.
    """
    (x, y) = localizacao
    return matriz[y][x]


def modificarElemento(matriz, localizacao, valor):
    """
    Modifica o elemento da matriz na localização dada.
    """
    (x, y) = localizacao
    matriz[y][x] = valor
    

def obterBordas(matriz):
    """
    Obtém os elementos que são bordas para a matriz de labirinto
    """
    # Obter tamanho da matriz de labirinto
    altura = len(matriz)
    comprimento = len(matriz[0])
    
    # Fazer uma lista de candidatos
    bordas = []
    for x in range(comprimento):
        bordas.append([x, 0])
        bordas.append([x, altura - 1])
    for y in range(altura):
        bordas.append([0, y])
        bordas.append([comprimento - 1, y])
        
    # Testar se os candidatos não estão nas paredes
    bordas_validas = []
    for borda in bordas:
        if borda not in bordas_validas:
            if checarLocalizacao(matriz, borda):
                bordas_validas.append(borda)
            
    # Voltar só quem é válido
    return bordas_validas


def checarLocalizacao(matriz, localizacao):
    """
    Verificar se a localização indicada é válida e não é uma pareda
    dada a matriz do labirinto. Retorna verdadeiro ou falso.
    """
    (x, y) = localizacao
    
    # Limites da matriz
    altura = len(matriz)
    comprimento = len(matriz[0])
    
    # Condições de contorno    
    condicao1 = (x < 0)
    condicao1 |= (y < 0)
    condicao1 |= (x >= comprimento)
    condicao1 |= (y >= altura)
    
    if condicao1:
        return False
    else:
        # Excluir elementos de parede
        elemento = obterElemento(matriz, localizacao)
        condicao2 = (elemento != -1)
        return condicao2


def descobrirVizinhos(matriz, localizacao): 
    """
    Verifica quais são os vizinhos da localização dada, excluindo-se
    as paredes. Retorna uma lista.
    """
    (x, y) = localizacao
    
    # Candidatos a vizinhos
    candidatos = [[x - 1, y], [x + 1, y], [x, y - 1], [x, y + 1]]
    vizinhos = []
    
    # Retornar somente quem não for parede
    for localizacao in candidatos:
        if checarLocalizacao(matriz, localizacao):
            vizinhos.append(localizacao)
    return vizinhos


def numeraVizinhos(lab, n, m, k):
    """
    Dada uma matriz lab com dimensões n x m, encontrar
    todos os elementos com valor k e em seguida
    mudar os valores de todos os vizinhos iguais a zero
    para k+1.
    """
    retorno = False 
    
    # Iterar na matriz
    for i in range(n):
        for j in range(m):
            localizacao = [j, i]
            elemento = obterElemento(lab, localizacao)
            
            if elemento == k:
                vizinhos = descobrirVizinhos(lab, localizacao)
                
                for vizinho in vizinhos:
                    val_vizinho = obterElemento(lab, vizinho)
                    if (val_vizinho == 0):
                        modificarElemento(lab, vizinho, k + 1)
                        retorno = True
    return retorno


def imprimirMaisDistante(lab, n, m, dist):
    """
    Descobrir a localização de todos os elementos
    cujo valor seja igual a dist.
    """
    for i in range(n):
        for j in range(m):
            localizacao = [j, i]
            elemento = obterElemento(lab, localizacao)
            
            if elemento == dist:
                print("{}\t".format(localizacao), end="")
                

def ilustrarMatriz(matriz):
    """
    Função para exibir a matriz de distâncias ou labirinto
    """
    for i, linha in enumerate(matriz):
        for elemento in linha:
            print("{:d}\t".format(elemento), end="")
        print()
    print()
    

def gerarCaminho(matriz, porta):
    """
    Dada uma matriz e uma tupla de porta, gerar a matriz de distâncias
    """
    
    # Dimensões da matriz
    n = len(matriz)
    m = len(matriz[0])
    
    # Colocar o valor do elemento de porta igual a 1
    modificarElemento(matriz, porta, 1)
    condicao = True
    k = 1
    
    # Numerar os vizinhos até não ser mais possível
    while (condicao == True):
        condicao = numeraVizinhos(matriz, n, m, k)
        k += 1
    
    distancia_maxima = k - 1
    
    # Ilustrar os resultados
    
    print("\n\nPorta {}:".format(porta), end="")
    
    print("\nCaminhos possíveis e comprimento: ")
    ilustrarMatriz(matriz)
    
    print("Posições com caminho de máximo comprimento:")
    imprimirMaisDistante(matriz, n, m, distancia_maxima)
    
    print("\n\nPosições sem caminho:")
    imprimirMaisDistante(matriz, n, m, 0)
    
    return matriz


def main(caminho_arquivo="teste.txt"):
    if len(sys.argv) == 2:
        caminho_arquivo = str(sys.argv[1])
    matriz_original = lerMatriz(caminho_arquivo)    
    portas = obterBordas(matriz_original)
    
    n = len(matriz_original)
    m = len(matriz_original[0])
    print("Matriz labirinto com {} linhas por {} colunas".format(n, m))
    ilustrarMatriz(matriz_original)
    print("Portas de entrada: {}".format(portas))

    for porta in portas:
        matriz = lerMatriz(caminho_arquivo)
        gerarCaminho(matriz, porta)


if __name__ == "__main__":
    main()
