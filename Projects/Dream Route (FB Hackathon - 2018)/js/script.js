var ini;
var fin;
var map;
var xi,yi,xf,yf;
var directionsDisplay;
var directionsService;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -23.587461, lng: -46.679610},
        zoom: 17
    });
    var input = /** @type {!HTMLInputElement} */(
        document.getElementById('pac-input1'));
    var input2 = /** @type {!HTMLInputElement} */(
        document.getElementById('pac-input2'));
    var types = document.getElementById('type-selector');
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);
    var fbBrasil = new google.maps.LatLng(-23.587202, -46.679557);
    var mapOptions = {
        zoom:14,
        center: fbBrasil
    }
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsService = new google.maps.DirectionsService();
    directionsDisplay.setMap(map);
    var autocomplete1 = new google.maps.places.Autocomplete(input);
    autocomplete1.bindTo('bounds', map);
    var autocomplete2 = new google.maps.places.Autocomplete(input2);
    autocomplete2.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete1.addListener('place_changed', function() {
        //window.alert("El primero de la lista era: " + infowindow[0].name);
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete1.getPlace();
        ini=place;
        str="" + place.geometry.location + "";
        var n = str.indexOf(",");
        xi=parseFloat(str.substr(1,n-1));
        yi=parseFloat(str.substr(n+2,str.length-n-3));
        //console.log(xi+yi);
        if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
        }
        
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
        } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setIcon(/** @type {google.maps.Icon} */({
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
        address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
    });

    autocomplete2.addListener('place_changed', function() {
        //window.alert("El primero de la lista era: " + infowindow[0].name);
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete2.getPlace();
        fin=place;
        str="" + place.geometry.location + "";
        var n = str.indexOf(",");
        xf=parseFloat(str.substr(1,n-1));
        yf=parseFloat(str.substr(n+2,str.length-n-3));
        if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
        }
        
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
        } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setIcon(/** @type {google.maps.Icon} */({
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
        address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
    });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
    function setupClickListener1(id, types) {
        var radioButton = document.getElementById('changetype-all');
        radioButton.addEventListener('click', function() {
        autocomplete1.setTypes(types);
        });
    }
    function setupClickListener2(id, types) {
        var radioButton = document.getElementById('changetype-all');
        radioButton.addEventListener('click', function() {
        autocomplete2.setTypes(types);
        });
    }
    setupClickListener1('changetype-all', []);
    setupClickListener2('changetype-all', []);
    
}
//onkeydown = "if (event.keyCode == 13) setupClickListener('changetype-all', []);"



function toList(str){
    str = str.replace("(","[");
    str = str.replace(")","]");
    return JSON.parse(str);
}
var wayp;
var wpoints=[];
function calcRoute() {
  var start = document.getElementById('pac-input1').value;
  var end = document.getElementById('pac-input2').value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            json_raw = xmlhttp.responseText;
            lista_pontos = toList(json_raw);
            console.log(lista_pontos);
            wpoints=[];
            for (coord in lista_pontos) {
                wpoints.push({location:(new google.maps.LatLng(lista_pontos[coord][0], lista_pontos[coord][1]))})
                console.log("coord: "+lista_pontos[coord][0]+" y "+lista_pontos[coord][1]);
            }
            console.log(wpoints)
            var request = {
            origin: start,
            destination: end,
            waypoints:wpoints,
            travelMode: 'DRIVING'
            };
            console.log("Terminó el request");
            directionsService.route(request, function(result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    console.log("ok");
                    directionsDisplay.setDirections(result);
                }
            });
        }
    }
    console.log(xi+","+yi);
    console.log(xf+","+yf);
    var selectedOption = $("input:radio[name=pref]:checked").val();
    xmlhttp.open("GET", "https://crossorigin.me/http://cefisma.org:5000/?x_i=" + xi + "&y_i=" + yi+"&x_f="+xf+"&y_f="+yf+"&tipo="+selectedOption, true);
    xmlhttp.send();
    
}
function ButtonClicked(){
    if(document.getElementById('pac-input1').value!="" && document.getElementById('pac-input2').value!="")
        calcRoute();
}
